import React from "react";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import "./App.scss";
import tareasJson from "./sample/task.json";
//import TasksComponent from "./components/Tasks";
import Login from "./views/Login.jsx";

class App extends React.Component {
  state = {
    tasks: tareasJson
  };

  render() {
    return (
      <div>
        <Login />
      </div>
    );
  }
}

export default App;
