import React from "react";
import ReactDOM from "react-dom";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import { BrowserRouter, Route } from "react-router-dom";
import Login from "./views/Login";
import Mainpage from "./views/Mainpage";
import Register from "./views/Register";
import RegisterAssignation from "./views/RegisterAssignation";
import Products from "./views/Products";
import Checkout from "./views/Checkout";
import "./index.css";

const App = () => (
  <BrowserRouter>
    <React.Fragment>
      <Route path="/Login" component={Login} />
      <Route path="/Mainpage" component={Mainpage} />
      <Route path="/Register" component={Register} />
      <Route path="/RegisterAssignation" component={RegisterAssignation} />
      <Route path="/Products" component={Products} />
      <Route path="/Checkout" component={Checkout} />
    </React.Fragment>
  </BrowserRouter>
);

ReactDOM.render(<App />, document.getElementById("root"));
