import React, { Component } from "react";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import "../design/Mainpage.scss";
import Navbar from "../components/navbar.jsx";
import Footer from "../components/FooterPage.jsx";

class Mainpage extends Component {
  render() {
    return (
      <body className="fondo_main">
        <div>
          <Navbar />
          <br />
          <center>
            <h1 className="texto">REACT WEB APP</h1>
            <p className="texto">Haz iniciado sesión.</p>
            <p className="texto">Para ir a otras páginas haz clic en</p>
            <p className="texto">alguna de las opciones de arriba.</p>
          </center>
        </div>
        <div className="footer">
          <Footer />
        </div>
      </body>
    );
  }
}

export default Mainpage;
