import React, { Component } from "react";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import "../design/Mainpage.scss";
import Navbar from "../components/navbar.jsx";
import Footer from "../components/FooterPage.jsx";
import { MDBContainer } from "mdbreact";
import Card from "../components/CheckoutComponents/Card";

class Checkout extends Component {
  render() {
    return (
      <body className="background">
        <Navbar />
        <br />
        <center>
          <MDBContainer className="container1">
            <Card />
          </MDBContainer>
        </center>
        <Footer />
      </body>
    );
  }
}

export default Checkout;
