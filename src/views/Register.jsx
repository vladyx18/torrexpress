import React, { Component } from "react";

import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import "../design/Register.scss";
import logo from "../images/logo-ltx.png";
import RegisterContent from "../components/RegistertComponent/RegisterContent";

class Register extends Component {
  render() {
    return (
      <body className="background">
        <center>
          <img src={logo} alt="logo" className="logo" />
          <div className="contenedor-espacio">
            <RegisterContent />
          </div>
        </center>
      </body>
    );
  }
}
export default Register;
