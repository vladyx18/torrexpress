import React, { Component } from "react";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import "../design/Products.scss";
import Navbar from "../components/navbar.jsx";
import Footer from "../components/FooterPage.jsx";
import Card from "../components/ProductsComponents/Card";
import Card2 from "../components/ProductsComponents/Card2";
import Pagination from "../components/ProductsComponents/Pagination";
import { MDBContainer } from "mdbreact";
//import Footer from "../components/FooterPage.jsx";

class Products extends Component {
  render() {
    return (
      <body className="background">
        <Navbar />
        <br />
        <center>
          <MDBContainer className="container1">
            <Card />
          </MDBContainer>
        </center>
        <center>
          <MDBContainer className="container2">
            <Card2 />
          </MDBContainer>
        </center>
        <br />
        <MDBContainer>
          <Pagination />
        </MDBContainer>
        <Footer />
      </body>
    );
  }
}

export default Products;
