import React, { Component } from "react";
//import { Helmet } from "react-helmet";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import "../design/Login.scss";
import logo from "../images/logo-ltx.png";
import LoginContent from "../components/LoginComponents/LoginContent";
import { MDBContainer } from "mdbreact";
import InvFooter from "../components/InvisibleFooter";

class Login extends Component {
  render() {
    return (
      <body className="fondo_login">
        <div>
          <div>
            <center>
              <img src={logo} alt="logo" className="logo" />
              <div className="contenedor-espacio">
                <LoginContent />
              </div>
            </center>
          </div>
        </div>
        <MDBContainer>
          <InvFooter />
        </MDBContainer>
      </body>
    );
  }
}

export default Login;
