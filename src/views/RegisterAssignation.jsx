import React, { Component } from "react";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import "../design/RegisterAssignation.scss";
import logo from "../images/logo-ltx.png";
import RegisterAssigContent from "../components/RegisterAssignationComponents/RegisterAssigContent";

class RegisterAssignation extends Component {
  render() {
    return (
      <body className="background">
        <div>
          <center>
            <img src={logo} alt="logo" className="logo" />
            <div className="contenedor-espacio">
              <RegisterAssigContent />
            </div>
          </center>
        </div>
      </body>
    );
  }
}

export default RegisterAssignation;
