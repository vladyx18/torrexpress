import React from "react";
import "../../design/Login.scss";
import { Link } from "react-router-dom";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBModalFooter,
  MDBBtn
} from "mdbreact";

const LoginContent = () => {
  return (
    <MDBContainer>
      <MDBRow>
        <MDBCol md="4" />
        <MDBCol md="4">
          <MDBCard>
            <MDBCardBody className="cardDesign">
              <h3 className="title primero">Bienvenido</h3>
              <h3 className="title segundo">Inicia Sesión</h3>
              <label htmlFor="defaultFormEmailEx" className="title tercero">
                Correo electronico
              </label>
              <input
                type="email"
                id="defaultFormEmailEx"
                className="form-control"
              />
              <br />
              <label htmlFor="defaultFormPasswordEx" className="title tercero">
                Contraseña
              </label>
              <input
                type="password"
                id="defaultFormPasswordEx"
                className="form-control"
              />

              <div className="text-center mt-4">
                <Link to="/Mainpage" className="title cuarto">
                  <MDBBtn
                    color="deep-orange"
                    className="mb-3 title cuarto"
                    type="submit"
                  >
                    Continuar
                  </MDBBtn>
                </Link>
              </div>

              <div>
                <Link to="/RegisterAssignation" className="title quinto">
                  Cambio Contraseña
                </Link>
              </div>

              <MDBModalFooter></MDBModalFooter>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
        <MDBCol md="4" />
      </MDBRow>
    </MDBContainer>
  );
};

export default LoginContent;
