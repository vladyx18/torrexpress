import React from "react";
import "../../design/RegisterAssignation.scss";
import { Link } from "react-router-dom";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBModalFooter,
  MDBBtn
} from "mdbreact";

const LoginContent = () => {
  return (
    <MDBContainer>
      <MDBRow>
        <MDBCol md="4" />
        <MDBCol md="4">
          <MDBCard>
            <MDBCardBody className="cardDesign">
              <h3 className="title primero">Bienvenido</h3>
              <h3 className="title segundo">Registra tu cuenta!</h3>
              <label htmlFor="defaultFormEmailEx" className="title tercero">
                Correo electronico
              </label>
              <input
                type="email"
                id="defaultFormEmailEx"
                className="form-control"
              />
              <br />
              <label htmlFor="defaultFormPasswordEx" className="title tercero">
                Contraseña
              </label>
              <input
                type="password"
                id="defaultFormPasswordEx"
                className="form-control"
              />
              <br />
              <label htmlFor="defaultFormPasswordEx2" className="title tercero">
                Confirmación Contraseña
              </label>
              <input
                type="password"
                id="defaultFormPasswordEx2"
                className="form-control"
              />
              <br />
              <div className="text-center mt-4">
                <MDBBtn
                  color="deep-orange"
                  className="mb-3 title cuarto"
                  type="submit"
                >
                  Registrar
                </MDBBtn>
              </div>

              <div>
                <Link to="/Login" className="title quinto">
                  Regresar a Login
                </Link>
              </div>

              <MDBModalFooter></MDBModalFooter>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
};

export default LoginContent;
