import React from "react";
import scott from "../../images/scott.png";
import dove from "../../images/dove.png";
import coke from "../../images/cocadieta.png";
import Dropdown from "../ProductsComponents/Dropdown.jsx";
import "../../design/Products.scss";
import {
  MDBBtn,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBCardTitle,
  MDBCardText,
  MDBRow,
  MDBCol
} from "mdbreact";

const Card2 = () => {
  return (
    <MDBRow>
      <MDBCol md="4">
        <MDBCard className="column">
          <MDBCardImage className="image" src={dove} waves />
          <MDBCardBody>
            <MDBCardTitle>Dove Original</MDBCardTitle>
            <MDBCardText>Jabon de cuerpo Dove original - Q15.00</MDBCardText>
            <Dropdown />
            <MDBBtn href="#" color="deep-purple darken-3" className="button">
              Añadir
            </MDBBtn>
          </MDBCardBody>
        </MDBCard>
      </MDBCol>
      <MDBCol md="4">
        <MDBCard className="column">
          <MDBCardImage className="image" src={scott} waves />
          <MDBCardBody>
            <MDBCardTitle>Scott x4 cuidado completo</MDBCardTitle>
            <MDBCardText>
              Fardo de 4 papeles higenicos marca Scott cuidado completo - Q25.00
            </MDBCardText>
            <Dropdown />
            <MDBBtn href="#" color="deep-purple darken-3" className="button">
              Añadir
            </MDBBtn>
          </MDBCardBody>
        </MDBCard>
      </MDBCol>
      <MDBCol md="4">
        <MDBCard className="column">
          <MDBCardImage className="image" src={coke} waves />
          <MDBCardBody>
            <MDBCardTitle>Coca-Cola Light Lata</MDBCardTitle>
            <MDBCardText>
              Coca-Cola Ligth presentacion en lata - Q6.00
            </MDBCardText>
            <Dropdown />
            <MDBBtn href="#" color="deep-purple darken-3" className="button">
              Añadir
            </MDBBtn>
          </MDBCardBody>
        </MDBCard>
      </MDBCol>
    </MDBRow>
  );
};

export default Card2;
