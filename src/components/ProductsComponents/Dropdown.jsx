import React from "react";
import "../../design/Products.scss";
import {
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem
} from "mdbreact";

const Dropdown = () => {
  return (
    <MDBDropdown className="dropdown">
      <MDBDropdownToggle caret color="warning">
        1
      </MDBDropdownToggle>
      <MDBDropdownMenu basic>
        <MDBDropdownItem>2</MDBDropdownItem>
        <MDBDropdownItem>3</MDBDropdownItem>
        <MDBDropdownItem>4</MDBDropdownItem>
        <MDBDropdownItem>5</MDBDropdownItem>
        <MDBDropdownItem>6</MDBDropdownItem>
        <MDBDropdownItem>7</MDBDropdownItem>
        <MDBDropdownItem>8</MDBDropdownItem>
        <MDBDropdownItem>9</MDBDropdownItem>
        <MDBDropdownItem>10</MDBDropdownItem>
      </MDBDropdownMenu>
    </MDBDropdown>
  );
};

export default Dropdown;
