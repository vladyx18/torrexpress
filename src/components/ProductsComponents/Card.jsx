import React from "react";
import hellmans from "../../images/hellmans.png";
import kerns from "../../images/kerns.png";
import naturas from "../../images/Naturas.png";
import Dropdown from "../ProductsComponents/Dropdown.jsx";
import "../../design/Products.scss";
import {
  MDBBtn,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBCardTitle,
  MDBCardText,
  MDBRow,
  MDBCol
} from "mdbreact";

const Card = () => {
  return (
    <MDBRow>
      <MDBCol md="4">
        <MDBCard className="column">
          <MDBCardImage className="image" src={hellmans} waves />
          <MDBCardBody>
            <MDBCardTitle>Mayonesa Hellman's Light</MDBCardTitle>
            <MDBCardText>
              Mayonessa Hellman's Light de 400g - Q30.00
            </MDBCardText>
            <Dropdown />
            <MDBBtn href="#" color="deep-purple darken-3" className="button">
              Añadir
            </MDBBtn>
          </MDBCardBody>
        </MDBCard>
      </MDBCol>
      <MDBCol md="4">
        <MDBCard className="column">
          <MDBCardImage className="image" src={kerns} waves />
          <MDBCardBody>
            <MDBCardTitle>Ketchup Kerns</MDBCardTitle>
            <MDBCardText>Salsa de tomate Kerns - Q15.00</MDBCardText>
            <Dropdown />
            <MDBBtn href="#" color="deep-purple darken-3" className="button">
              Añadir
            </MDBBtn>
          </MDBCardBody>
        </MDBCard>
      </MDBCol>
      <MDBCol md="4">
        <MDBCard className="column">
          <MDBCardImage className="image" src={naturas} waves />
          <MDBCardBody>
            <MDBCardTitle>Natura's Americana</MDBCardTitle>
            <MDBCardText>
              Salsa de tomate estilo Americana Natura's - Q5.00
            </MDBCardText>
            <Dropdown />
            <MDBBtn href="#" color="deep-purple darken-3" className="button">
              Añadir
            </MDBBtn>
          </MDBCardBody>
        </MDBCard>
      </MDBCol>
    </MDBRow>
  );
};

export default Card;
