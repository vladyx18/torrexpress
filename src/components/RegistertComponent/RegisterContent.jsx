import React from "react";
import { MDBContainer, MDBRow, MDBCol, MDBBtn } from "mdbreact";
import "../../App.scss"; //Import here your file style

import { MDBInput } from "mdbreact";

const gridExamplesPage = () => {
  return (
    <MDBContainer className="cardDesign">
      <MDBRow>
        <MDBCol md="6">
          {" "}
          <label htmlFor="defaultFormNamex" className="title tercero">
            Nombres
          </label>
          <input type="text" id="defaultFormNamex" className="form-control" />
        </MDBCol>
        <MDBCol md="6">
          <label htmlFor="defaultFormLastNamex" className="title tercero">
            Apellidos
          </label>
          <input
            type="text"
            id="defaultFormLastNamex"
            className="form-control"
          />
        </MDBCol>
        <MDBCol md="6">
          {" "}
          <label htmlFor="defaultFormNit" className="title tercero">
            Nit
          </label>
          <input type="text" id="defaultFormNit" className="form-control" />
        </MDBCol>
        <MDBCol md="6">
          <label htmlFor="defaultFormEmailEx" className="title tercero">
            Correo electronico
          </label>
          <input
            type="email"
            id="defaultFormEmailEx"
            className="form-control"
          />
        </MDBCol>

        <MDBCol md="6">
          {" "}
          <label htmlFor="defaultFormTel" className="title tercero">
            Telefono
          </label>
          <input type="number" id="defaultFormTel" className="form-control" />
        </MDBCol>

        <MDBCol md="6">
          {" "}
          <label htmlFor="defaultFormDirec" className="title tercero">
            Direcccion
          </label>
          <input type="text" id="defaultFormDirec" className="form-control" />
        </MDBCol>

        <MDBCol md="6" className="title">
          {" "}
          Administrador
          <MDBInput filled type="checkbox" id="checkbox1" />
        </MDBCol>

        <MDBCol md="6" className="title">
          {" "}
          Usuario
          <MDBInput filled type="checkbox" id="checkbox2" />
        </MDBCol>

        <MDBCol md="12" className="Checkbox">
          <center>
            <MDBBtn
              color="deep-orange"
              className="mb-6 title cuarto"
              type="submit"
            >
              Registrar
            </MDBBtn>
          </center>
        </MDBCol>

        <MDBCol md="12">
          <center>
            <label htmlFor="" className="title quinto">
              Regresar a Login
            </label>
          </center>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
};

export default gridExamplesPage;
