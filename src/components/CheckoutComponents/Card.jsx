import React, { Component } from "react";
import {
  MDBBtn,
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBContainer,
  MDBTable,
  MDBTableBody,
  MDBTableHead,
  MDBCol,
  MDBModal,
  MDBModalBody,
  MDBModalHeader,
  MDBModalFooter
} from "mdbreact";

class Card extends Component {
  state = {
    modal: false
  };

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  };

  render() {
    return (
      <MDBCol>
        <MDBCard style={{ width: "44rem" }}>
          <MDBCardBody>
            <MDBCardTitle>Compra</MDBCardTitle>
            <MDBCardText>
              <MDBContainer>
                <MDBTable>
                  <MDBTableHead>
                    <tr>
                      <th>#</th>
                      <th>Producto</th>
                      <th>Cantidad</th>
                      <th>Subtotal</th>
                      <th></th>
                    </tr>
                  </MDBTableHead>
                  <MDBTableBody>
                    <tr>
                      <td>1</td>
                      <td>HELLMANS-MAYO</td>
                      <td>2</td>
                      <td>Q60.00</td>
                      <td>
                        <MDBBtn color="danger" href="#">
                          Eliminar
                        </MDBBtn>
                      </td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>COCA-LIGHT</td>
                      <td>3</td>
                      <td>Q18.00</td>
                      <td>
                        <MDBBtn color="danger" href="#">
                          Eliminar
                        </MDBBtn>
                      </td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>DOVE-JABON</td>
                      <td>1</td>
                      <td>Q15.00</td>
                      <td>
                        <MDBBtn color="danger" href="#">
                          Eliminar
                        </MDBBtn>
                      </td>
                    </tr>
                    <MDBCardText>TOTAL: Q93.00</MDBCardText>
                  </MDBTableBody>
                </MDBTable>
              </MDBContainer>
            </MDBCardText>
            <MDBBtn color="primary" onClick={this.toggle}>
              Realizar compra
            </MDBBtn>
            <MDBModal isOpen={this.state.modal} toggle={this.toggle}>
              <MDBModalHeader toggle={this.toggle}>
                Finalizar la compra
              </MDBModalHeader>
              <MDBModalBody>
                <MDBCol md="8">
                  <figure className="figure">
                    <img
                      src="https://images.samsung.com/is/image/samsung/p5/au/faq/os-pie-updates/QR-code.png?$ORIGIN_PNG$"
                      className="figure-img img-fluid z-depth-1"
                      alt=""
                      style={{ width: "800px" }}
                    />
                    <figcaption className="figure-caption">
                      Escanear esto para finalizar la compra.
                    </figcaption>
                  </figure>
                </MDBCol>
              </MDBModalBody>
              <MDBModalFooter>
                <MDBBtn color="success" onClick={this.toggle}>
                  Cerrar
                </MDBBtn>
              </MDBModalFooter>
            </MDBModal>
          </MDBCardBody>
        </MDBCard>
      </MDBCol>
    );
  }
}

export default Card;
