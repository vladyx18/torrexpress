import React from "react";
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";
import "../design/Mainpage.scss";

const FooterPage = () => {
  return (
    <MDBFooter className="font-small pt-4 mt-4 colorcito footer">
      <MDBContainer fluid className="text-center text-md-left">
        <MDBRow>
          <MDBCol md="6">
            <h5 className="white-text">TorrExpress</h5>
            <p>Esta plataforma es propiedad de La Torre.</p>
          </MDBCol>
          <MDBCol md="6">
            <h5 className="white-text">Enlaces directos</h5>
            <ul>
              <li className="list-unstyled, textoMorado">
                <a href="https://www.supermercadoslatorre.com/web/index.php/ofertas">
                  Ofertas
                </a>
              </li>
              <li className="list-unstyled, textoMorado">
                <a href="https://www.supermercadoslatorre.com/web/index.php/blog">
                  Blog
                </a>
              </li>
              <li className="list-unstyled, textoMorado">
                <a href="https://www.supermercadoslatorre.com/web/index.php/ubicaciones">
                  Ubicaciones
                </a>
              </li>
              <li className="list-unstyled, textoMorado">
                <a href="https://www.supermercadoslatorre.com/web/index.php/conocenos">
                  Conócenos
                </a>
              </li>
            </ul>
          </MDBCol>
        </MDBRow>
      </MDBContainer>
      <div className="footer-copyright text-center py-3">
        <MDBContainer fluid>
          &copy; {new Date().getFullYear()} Copyright:{" "}
          <a href="https://umg.edu.gt"> umg.edu.gt </a>
        </MDBContainer>
      </div>
    </MDBFooter>
  );
};

export default FooterPage;
